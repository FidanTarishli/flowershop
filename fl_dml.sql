insert into fl_customers values (1,'Khumar','Mirzayeva','Baku',0519088201);
commit;
insert into fl_customers values (2,'Madina','Qulizada','Baku',0508752252);
commit;
insert into fl_customers values (3,'Sabina','Abasova','Baku',0709721005);
commit;
insert into fl_customers values (4,'Fidan','Tarishli','Baku',0503625575);  
commit;


insert into fl_suppliers values(1,'Arif','Lankaran');
commit;
insert into fl_suppliers values(2,'Baxtiyar','Xachmaz');
commit;
insert into fl_suppliers values(3,'Ceyran','Gandja');
commit;
insert into fl_suppliers values(4,'Gunay','Baku');
commit;

insert into  fl_branches values(1,'Albert Aqarunov kuchesi',0505394393);
commit;
insert into  fl_branches values(2,'Ash?q Molla Cüm? kuchesi',0105121316);
commit;
insert into  fl_branches values(3,'Akim Abbasov kuchesi',0702004537);
commit;
insert into  fl_branches values(4,'Afiy?ddin C?lilov kuchesi',0507994393);
commit;

insert into fl_flowers values(1,'Q?z?lgul',15,3);
commit;
insert into fl_flowers values(2,'Lavanda',10,1);
commit;
insert into fl_flowers values(3,'Lale',5,2);
commit;
insert into fl_flowers values(4,'Tulpan',20,4);
commit;


insert into fl_orders values(1,3,4,TO_DATE('2023-05-29', 'YYYY-MM-DD'));
commit;
insert into fl_orders values(2,1,2,TO_DATE('2023-05-28', 'YYYY-MM-DD'));
commit;
insert into fl_orders values(3,4,1,TO_DATE('2023-05-27', 'YYYY-MM-DD'));
commit;
insert into fl_orders values(4,2,3,TO_DATE('2023-05-26', 'YYYY-MM-DD'));
commit;



insert into fl_orderdetails values(1,3,5);
commit;
insert into fl_orderdetails values(2,4,8);
commit;
insert into fl_orderdetails values(3,1,1);
commit;
insert into fl_orderdetails values(4,2,4);
commit;
